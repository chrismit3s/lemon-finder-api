from flask import Flask, Response, abort, g, jsonify, redirect, render_template, request, send_file, send_from_directory
from htmlmin.decorator import htmlmin as minify
from http import HTTPStatus
from multiprocessing import cpu_count
from os.path import join
from src import all_classes, app_filename, colors, labels, plot_period, port, refresh_delay, use_tls
from src.data import Database, get_connection_qrcode
import os


app = Flask(__name__,
            static_folder=join("..", "static"),
            template_folder=join("..", "templates"))


@app.before_request
def setup_db():
    g.db = Database()


@app.teardown_appcontext
def close_db(*args, **kwargs):
    del g.db


@app.route("/js/<path:path>")
def js(path):
    return send_from_directory(app.static_folder, "js/" + path)


@app.route("/css/<path:path>")
def css(path):
    return send_from_directory(app.static_folder, "css/" + path)


@app.route("/fonts/<path:path>")
def fonts(path):
    return send_from_directory(app.static_folder, "fonts/" + path)


@app.route("/qrcode", methods=["GET"])
def qrcode():
    return Response(
        get_connection_qrcode(),
        mimetype="image/svg+xml")


@app.route("/download", methods=["GET"])
def download():
    return send_file(app_filename, as_attachment=True)


@app.route("/register", methods=["POST"])
def register():
    try:
        print("log called with", request.json)
        return jsonify(token=g.db.register_user(request.json["name"]))
    except KeyError:
        abort(HTTPStatus.FORBIDDEN)


@app.route("/log", methods=["POST"])
def log():
    try:
        print("log called with", request.json)
        g.db.log(request.json["token"], request.json["class"])
        return jsonify({})
    except KeyError:  # bad token
        abort(HTTPStatus.FORBIDDEN)
    except KeyError:  # bad class
        abort(HTTPStatus.BAD_REQUEST)


@app.route("/stats", methods=["GET"])
@minify(remove_empty_space=True)
def stats():
    return render_template("index.html.jinja",
                           refresh_delay=refresh_delay,
                           piedata=get_piedata(),
                           linedata=get_linedata(),
                           events_str=get_events_str())


@app.route("/", methods=["GET"])
def index():
    if request.user_agent.platform == "android":  # only android phones have use for the apk
        return redirect("/download")
    else:
        return redirect("/stats")


def get_piedata():
    counts = g.db.get_scan_counts(period=plot_period)

    values = [counts[scanned_class] for scanned_class in all_classes]
    if set(values) == {0}:  # no data (only zeros)
        values = [0] * (len(all_classes) - 1) + [1]

    piedata = [{
        "values": values,
        "labels": labels,
        "marker": {
            "colors": colors,
        },
        "type": "pie",
    }]

    return piedata


@app.route("/data/pie-chart", methods=["GET"])
def data_piechart():
    return jsonify(get_piedata())


def get_linedata():
    linedata = []
    for scanned_class, label, color in zip(all_classes, labels, colors):
        histogram, bin_edges = g.db.get_scans_over_time(scanned_class, step=refresh_delay, period=plot_period)
        linedata.append({
            "x": list(float(x) for x in bin_edges[:-1]),
            "y": list(float(x) for x in histogram),
            "name": label,
            "mode": "lines",
            "marker": {
                "color": color,
            },
        })
    return linedata


@app.route("/data/line-plot", methods=["GET"])
def data_lineplot():
    return jsonify(get_linedata())


def get_events_str():
    event_lines = []
    for event in g.db.events[::-1]:
        event_line = "<div class='event-box-tile'>"
        if hasattr(event, "scanned_class"):
            event_line += f"<b>{event.username}</b> scanned {event.scanned_class}; <code>{event.token}</code>."
        else:
            event_line += f"<b>{event.username}</b> registered for token <code>{event.token}</code>."
        event_line += "</div>"

        event_lines.append(event_line)

    return "<hr />".join(event_lines)


@app.route("/data/events", methods=["GET"])
def data_events():
    return Response(get_events_str(), mimetype="text/plain")


if __name__ == "__main__":
    # number of workers (see https://docs.gunicorn.org/en/stable/design.html#how-many-workers(
    workers = f"--workers {2 * cpu_count() + 1}"

    # 5min timeout as downloading the apk can take a while
    timeout = "--timeout 300"

    # usually we dont use tls, the app doesnt support it
    certs = "--certfile cert/cert.pem --keyfile cert/key.pem" if use_tls else ""

    # set port
    bind = f"--bind 0.0.0.0:{port}"

    # authbind requires the current user to own the file in /etc/authbind/byport/<port> (see https://mutelight.org/authbind)
    command = " ".join(("DEBUG=0 authbind gunicorn", workers, timeout, certs, bind, "'src.app:app'"))

    # reset the database
    print("reinitializing the database...", end="\r", flush=True)
    Database().reset()
    print("reinitializing the database - done")

    # start gunicorn
    os.system(command)
