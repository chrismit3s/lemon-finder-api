from collections import Counter, namedtuple
from datetime import datetime
from functools import cached_property
from io import BytesIO
from src import db_filename, all_classes, port
from uuid import uuid4
import numpy as np
import os
import pyqrcode as qr
import socket as st
import sqlite3 as sql
import subprocess as sp


ScanEvent = namedtuple("ScanEvent", ["created", "token", "username", "scanned_class"])
RegisterEvent = namedtuple("RegisterEvent", ["created", "token", "username"])


class Database():
    def __init__(self):
        self._conn = None
        self._cur = None

    def __del__(self):
        if self._conn is not None:
            self._conn.close()

    @property
    def conn(self):
        if self._conn is None:
            self._conn = sql.connect(db_filename, detect_types=sql.PARSE_DECLTYPES)
            self._conn.row_factory = sql.Row  # returns namedtuples
            self._cur = self._conn.cursor()
        return self._conn

    @property
    def cur(self):
        if self._cur is None:
            _ = self.conn  # accessing the conn attr also creates a cursor
        return self._cur

    @cached_property  # cached as this Database object only lives for one request, so changes dont really matter
    def user_lookup(self):
        rows = self.query("SELECT token, username FROM users")
        return {row["token"]: row["username"] for row in rows}

    @property
    def scan_events(self):
        rows = self.query("SELECT user_token, class_index, scanned_on FROM scans;")
        gen = (ScanEvent(created=row["scanned_on"],
                         token=row["user_token"],
                         username=self.user_lookup[row["user_token"]],
                         scanned_class=all_classes[row["class_index"]])
               for row in rows)
        return sorted(gen, key=lambda x: x.created)

    @property
    def register_events(self):
        rows = self.query("SELECT token, username, registered_on FROM users;")
        gen = (RegisterEvent(created=row["registered_on"],
                             token=row["token"],
                             username=row["username"])
               for row in rows)
        return sorted(gen, key=lambda x: x.created)

    @property
    def events(self):
        return sorted(self.register_events + self.scan_events, key=lambda x: x.created)

    def query(self, query):
        self.cur.execute(query)
        return self.cur.fetchall()

    def update(self, query):
        self.cur.execute(query)
        self.conn.commit()

    def reset(self):
        with open("src/schema.sql") as schema:
            self.conn.executescript(schema.read())

    def register_user(self, username):
        if username in self.user_lookup.values():
            raise KeyError(f"{username} is already registered")

        token = "{" + str(uuid4()) + "}"
        self.update(f"INSERT INTO users(username, token) VALUES ('{username}', '{token}');")

        print(f"reg {token} {username}")
        return token

    def log(self, token, scanned_class):
        if token not in self.user_lookup.keys():
            raise KeyError(f"No or invalid token provided ('{token}')")
        if scanned_class not in all_classes:
            raise ValueError(f"{scanned_class} is not a valid class")

        print(f"log {token} {scanned_class}")
        class_index = all_classes.index(scanned_class)
        self.update(f"INSERT INTO scans(user_token, class_index) VALUES ('{token}', '{class_index}');")

    def get_scan_counts(self, period):
        now = datetime.utcnow()
        scanned_classes = [event.scanned_class
                           for event in self.scan_events
                           if ((now - event.created).total_seconds() < period)]
        return Counter(scanned_classes)

    def get_scans_over_time(self, scanned_class, step, period):
        now = datetime.utcnow()
        scan_times = [(event.created - now).total_seconds()  # stores negative numbers (how far back in time each scan is)
                      for event in self.scan_events
                      if event.scanned_class == scanned_class]
        return np.histogram(scan_times, bins=period // step, range=(-period, 0))


def get_host_ip():
    if os.name == "nt":
        return st.gethostbyname(st.gethostname())
    else:
        output = sp.run(["hostname", "-I"], stdout=sp.PIPE).stdout
        ips = output.decode("utf-8").split()
        return ips[0]


def get_connection_qrcode():
    qrcode = qr.create(get_host_ip() + ":" + str(port), error="H")
    output = BytesIO()
    qrcode.svg(output, background="#FFFFFF")
    return output.getvalue()


if __name__ == "__main__":
    if input("Do you really want to reset the database? ").lower() == "y":
        Database().reset()
        print("Reinitialized the database.")
