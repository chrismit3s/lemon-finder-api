from os.path import join


app_filename = join("..", "static", "app.apk")  # path relative to src/app.py
db_filename = join("database", "database.db")  # path relative to project root

use_tls = False
port = 80

refresh_delay = 2  # seconds
plot_period = 120  # seconds

classes = ["apple", "banana", "lemon"]
no_match_class = "none-of-the-above"

all_classes = classes + [no_match_class]

labels = [
    u"Apple\U0001F34E",  # red apple emoji
    u"Banana\U0001F34C",  # banana emoji
    u"Lemon\U0001F34B",  # lemon emoji
    u"No match\U0001F937\U0000200D\U00002642\U0000FE0F",  # man shrugging emoji
]
colors = [
    "#CC2936",
    "#F9CB40",
    "#6DA34D",
    "#0D3B66",
]
