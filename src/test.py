from contextlib import suppress
from src import port
import requests as rq


urlbase = f"http://localhost:{port}/"


def register(name):
    res = rq.post(urlbase + "register", json={"name": name})
    return res.json()["token"]


def log(token, class_):
    rq.post(urlbase + "log", json={"token": token, "class": class_})


if __name__ == "__main__":
    user_mapping = {}
    with suppress(KeyboardInterrupt):
        while True:
            line = input("> ")
            cmd, *args = line.split(" ")
            if cmd[0] == "r":  # register
                name = args[0]
                token = register(name)
                user_mapping[name] = token
                print(f"registered username {name} for token {token}")

            elif cmd[0] == "l":  # log
                name = args[0]
                class_ = args[1]
                if name in user_mapping:
                    log(user_mapping[name], class_)
                    print(f"logged a scan of class {class_} as username {name}")
                else:
                    print(f"username {name} not registered")

            elif cmd[0] == "c":  # clear
                user_mapping = {}
                print("cleared all previous users")

            elif cmd[0] == "p":  # print
                print("registered users:")
                print("\n".join(f"  {token}: {name}" for name, token in user_mapping.items()))
