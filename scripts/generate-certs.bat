@echo off

cd cert

openssl genrsa -des3 -out local.key 1024
openssl req -new -key local.key -out local.csr

copy local.key local.key.orig
openssl rsa -in local.key.orig -out local.key

openssl x509 -req -days 365 -in local.csr -signkey local.key -out local.crt

cd ..
